package com.anggorotri.moviesandtvwapi.retrofit;

import com.anggorotri.moviesandtvwapi.items_response.MoviesResponse;
import com.anggorotri.moviesandtvwapi.items_response.TVShowResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {
   // String BASE_URL = "https://api.themoviedb.org/3/discover/";
    //String API_KEY = "c831506e168639cf3884d77eb4de6adc";
   // String LANGUAGE = "&language=en-US";

    @GET("movie")
    Call<MoviesResponse> getMovies(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page
    );
    @GET("tv")
    Call<TVShowResponse> getTvShow(
            @Query("api_key") String apiKey,
            @Query("language") String language,
            @Query("page") int page
    );

}
