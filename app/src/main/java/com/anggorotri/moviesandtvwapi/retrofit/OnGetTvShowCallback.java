package com.anggorotri.moviesandtvwapi.retrofit;

import com.anggorotri.moviesandtvwapi.items_response.TVShowItems;

import java.util.List;

public interface OnGetTvShowCallback {

    void onSuccess(List<TVShowItems> tvShowItems);

    void onError();

}
