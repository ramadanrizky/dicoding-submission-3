package com.anggorotri.moviesandtvwapi.retrofit;

import com.anggorotri.moviesandtvwapi.items_response.MoviesItems;

import java.util.List;

public interface OnGetMoviesCallback {

    void onSuccess(List<MoviesItems> moviesItems);

    void  onError();

}
