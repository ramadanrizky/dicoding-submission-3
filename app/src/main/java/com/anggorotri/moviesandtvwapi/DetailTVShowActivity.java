package com.anggorotri.moviesandtvwapi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anggorotri.moviesandtvwapi.items_response.TVShowItems;
import com.bumptech.glide.Glide;

public class DetailTVShowActivity extends AppCompatActivity {
    public static final String EXTRA_TVSHOW = "extra_tvshow";
    private String url = "https://image.tmdb.org/t/p/original";
    TextView txt_titletv, txt_datetv, txt_descriptiontv, txt_toolbar_tv;
    ImageView img_detailtv;
    ProgressBar pb_detail_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tvshow);

        txt_titletv = findViewById(R.id.txt_detail_tv);
        txt_toolbar_tv = findViewById(R.id.txt_toolbar_tv);
        txt_datetv = findViewById(R.id.txt_detail_date_tv);
        txt_descriptiontv =findViewById(R.id.txt_detail_description_tv);
        img_detailtv = findViewById(R.id.img_detail_tv);
        pb_detail_tv = findViewById(R.id.pb_detail_tv);
        pb_detail_tv.setVisibility(View.VISIBLE);

        getDetailTvShow();

    }

    private void getDetailTvShow() {
        TVShowItems items = getIntent().getParcelableExtra(EXTRA_TVSHOW);
        txt_titletv.setText(items.getName());
        txt_datetv.setText(items.getFirst_air_date());
        txt_descriptiontv.setText(items.getOverview());
        txt_toolbar_tv.setText(items.getName());
        Glide.with(DetailTVShowActivity.this)
                .load(url + items.getPoster_path())
                .into(img_detailtv);

        pb_detail_tv.setVisibility(View.GONE);
    }
}
