//
//package com.anggorotri.moviesandtvwapi.viewmodel;
//
//import android.arch.lifecycle.LiveData;
//import android.arch.lifecycle.MutableLiveData;
//import android.arch.lifecycle.ViewModel;
//
//import com.anggorotri.moviesandtvwapi.items_response.MoviesResponse;
//import com.anggorotri.moviesandtvwapi.repo.MoviesRepo;
//import com.anggorotri.moviesandtvwapi.retrofit.OnGetMoviesCallback;
//
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;
//
//
//public class Model extends ViewModel {
//    private MutableLiveData<MoviesResponse> mutableLiveData;
//    private MoviesRepo moviesRepo;
//
//    public void init(){
//        if (mutableLiveData != null){
//            return;
//        }
//        moviesRepo = MoviesRepo.getInstance();
//        mutableLiveData = moviesRepo.getMovies("c831506e168639cf3884d77eb4de6adc","en-US",1);
//
//    }
//    public LiveData<MoviesResponse> getMoviesRepo() {
//        return mutableLiveData;
//    }
//}
