package com.anggorotri.moviesandtvwapi;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.anggorotri.moviesandtvwapi.adapter.TVShowAdapter;
import com.anggorotri.moviesandtvwapi.decoration_grid.GridSpacingItemDecoration;
import com.anggorotri.moviesandtvwapi.items_response.TVShowItems;
import com.anggorotri.moviesandtvwapi.repo.TVShowRepo;
import com.anggorotri.moviesandtvwapi.retrofit.OnGetTvShowCallback;

import java.util.List;

public class TVShowFragment extends Fragment implements TVShowAdapter.TvClickListener {

    RecyclerView rv_grid_tv;
    TVShowAdapter tvAdapter;
    private TVShowRepo tvShowRepo;
    ProgressBar pb_tv;


    public TVShowFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_tvshow, container, false);

        tvShowRepo = TVShowRepo.getInstance();
        pb_tv = rootView.findViewById(R.id.pb_tv);

        rv_grid_tv = rootView.findViewById(R.id.rv_grid_tv);
        RecyclerView.LayoutManager manager = new GridLayoutManager(getActivity(), 2);
        rv_grid_tv.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(3), true));
        rv_grid_tv.setItemAnimator(new DefaultItemAnimator());
        rv_grid_tv.setLayoutManager(manager);
        rv_grid_tv.setHasFixedSize(true);

        pb_tv.setVisibility(View.VISIBLE);
        dataTV();

        return rootView;
    }

    private void dataTV() {
        tvShowRepo.getTvShow(new OnGetTvShowCallback() {
            @Override
            public void onSuccess(List<TVShowItems> tvShowItems) {
                tvAdapter = new TVShowAdapter(tvShowItems);
                rv_grid_tv.setAdapter(tvAdapter);
                pb_tv.setVisibility(View.GONE);
                tvAdapter.setClickListener(TVShowFragment.this::tvItemClicked);
            }

            @Override
            public void onError() {
                Toast.makeText(getActivity(), "check internet", Toast.LENGTH_SHORT).show();
                pb_tv.setVisibility(View.GONE);
            }
        });
    }


    private int dpToPx(int dp) {
        Resources resources = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics()));
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }



    @Override
    public void tvItemClicked(TVShowItems setGet, int position, View v) {
        TVShowItems items = new TVShowItems();
        items.setName(setGet.getName());
        items.setFirst_air_date(setGet.getFirst_air_date());
        items.setOverview(setGet.getOverview());
        items.setPoster_path(setGet.getPoster_path());

        Intent i = new Intent(getActivity(), DetailTVShowActivity.class);
        i.putExtra(DetailTVShowActivity.EXTRA_TVSHOW,items);
        startActivity(i);
    }
}
