package com.anggorotri.moviesandtvwapi.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anggorotri.moviesandtvwapi.R;
import com.anggorotri.moviesandtvwapi.items_response.MoviesItems;
import com.anggorotri.moviesandtvwapi.items_response.TVShowItems;
import com.bumptech.glide.Glide;

import java.util.List;

public class TVShowAdapter extends RecyclerView.Adapter<TVShowAdapter.TvShowViewHolder> {

    List<TVShowItems> TvShowList;
    private String url = "https://image.tmdb.org/t/p/original";
    private TvClickListener clickListener;

    public TVShowAdapter(List<TVShowItems> TvShowIList){
        this.TvShowList = TvShowIList;
    }

    public void setClickListener(TvClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface TvClickListener {
        void tvItemClicked(TVShowItems setGet, int position, View v);
    }

    @NonNull
    @Override
    public TvShowViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_tv, viewGroup, false);
        return new TvShowViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TvShowViewHolder tvShowViewHolder, int i) {
        tvShowViewHolder.bind(TvShowList.get(i));
    }

    @Override
    public int getItemCount() {
        return (TvShowList != null) ? TvShowList.size() : 0;
    }

    public class TvShowViewHolder extends RecyclerView.ViewHolder {

        TextView txt_title_tv, txt_date_tv, txt_overview_tv;
        ImageView img_tv;

        public TvShowViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_title_tv = itemView.findViewById(R.id.txt_titletv);
            txt_date_tv = itemView.findViewById(R.id.txt_datetv);
            txt_overview_tv = itemView.findViewById(R.id.txt_descriptiontv);
            img_tv = itemView.findViewById(R.id.img_tv);
        }

        public void bind(TVShowItems tvShowItems) {
            txt_title_tv.setText(tvShowItems.getName());
            txt_date_tv.setText(tvShowItems.getFirst_air_date());
            txt_overview_tv.setText(tvShowItems.getOverview());
            Glide.with(itemView)
                    .load(url+tvShowItems.getPoster_path())
                    .into(img_tv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null){
                        clickListener.tvItemClicked(TvShowList.get(getPosition()), getPosition(), v);
                    }
                }
            });
        }
    }
}
