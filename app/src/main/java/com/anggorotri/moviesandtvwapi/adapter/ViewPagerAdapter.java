package com.anggorotri.moviesandtvwapi.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.widget.Switch;

import com.anggorotri.moviesandtvwapi.MoviesFragment;
import com.anggorotri.moviesandtvwapi.TVShowFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    int numbTabs;

    public ViewPagerAdapter(FragmentManager fm, int numbTabs) {
        super(fm);
        this.numbTabs = numbTabs;
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                MoviesFragment movieFM = new MoviesFragment();
                return movieFM;
            case 1:
                TVShowFragment tvShowFM = new TVShowFragment();
                return tvShowFM;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numbTabs;
    }
}
