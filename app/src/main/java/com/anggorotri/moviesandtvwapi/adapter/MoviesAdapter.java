package com.anggorotri.moviesandtvwapi.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.anggorotri.moviesandtvwapi.R;
import com.anggorotri.moviesandtvwapi.items_response.MoviesItems;
import com.bumptech.glide.Glide;

import java.util.List;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {


    List<MoviesItems> moviesList;
    private String url = "https://image.tmdb.org/t/p/original";
    private ClickListener clickListener;

    public MoviesAdapter(List<MoviesItems> moviesList){

        this.moviesList = moviesList;
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void itemClicked(MoviesItems setGet, int position, View v);
    }

    @NonNull
    @Override
    public MoviesViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.grid_movies, viewGroup, false);
        return new MoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesViewHolder moviesViewHolder, int i) {
       moviesViewHolder.bind(moviesList.get(i));
    }

    @Override
    public int getItemCount() {
        return (moviesList != null) ? moviesList.size() : 0;
    }

    public class MoviesViewHolder extends RecyclerView.ViewHolder {

        TextView txt_title_movies, txt_date_movies, txt_description_movies;
        ImageView img_movies;


        public MoviesViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_title_movies = itemView.findViewById(R.id.txt_titlemovie);
            txt_date_movies = itemView.findViewById(R.id.txt_datemovie);
            txt_description_movies = itemView.findViewById(R.id.txt_descriptionmovie);
            img_movies = itemView.findViewById(R.id.img_movie);

//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    if (clickListener != null){
//                        clickListener.itemClicked(moviesList.get(getPosition()), getPosition(), v);
//                    }
//                }
//            });

        }

        public void bind(MoviesItems moviesItems) {

            txt_title_movies.setText(moviesItems.getTitle());
            txt_date_movies.setText(moviesItems.getRelease_date());
            txt_description_movies.setText(moviesItems.getOverview());
            Glide.with(itemView)
                    .load(url + moviesItems.getPoster_path())
                    .into(img_movies);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (clickListener != null){
                        clickListener.itemClicked(moviesList.get(getPosition()), getPosition(), v);
                    }
                }
            });

        }

    }
}
