package com.anggorotri.moviesandtvwapi;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.anggorotri.moviesandtvwapi.adapter.MoviesAdapter;
import com.anggorotri.moviesandtvwapi.decoration_grid.GridSpacingItemDecoration;
import com.anggorotri.moviesandtvwapi.items_response.MoviesItems;
import com.anggorotri.moviesandtvwapi.repo.MoviesRepo;
import com.anggorotri.moviesandtvwapi.retrofit.OnGetMoviesCallback;
//import com.anggorotri.moviesandtvwapi.viewmodel.Model;
//import com.anggorotri.moviesandtvwapi.viewmodel.Model;

import java.util.ArrayList;
import java.util.List;


public class MoviesFragment extends Fragment implements MoviesAdapter.ClickListener {

    RecyclerView rv_grid_movies;
    MoviesAdapter moviesAdapter;
//    private Model viewModel;
    ArrayList<MoviesItems> moviesItemsArrayList = new ArrayList<>();
  //  Context context;
    private MoviesRepo moviesRepo;
    ProgressBar pb;

    public MoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_movies, container, false);

        pb = rootView.findViewById(R.id.pb);
        rv_grid_movies = rootView.findViewById(R.id.rv_grid_movies);

//        viewModel = ViewModelProviders.of(this).get(Model.class);
//        viewModel.init();
//        viewModel.getMoviesRepo().observe(this, moviesResponse -> {
//            List<MoviesItems> moviesItems = moviesResponse.getMoviesItems();
//            moviesItemsArrayList.addAll(moviesItems);
//            moviesAdapter.notifyDataSetChanged();
//        });

        RecyclerView.LayoutManager manager = new GridLayoutManager(getActivity(), 2);
        rv_grid_movies.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(3), true));
        rv_grid_movies.setItemAnimator(new DefaultItemAnimator());
        rv_grid_movies.setLayoutManager(manager);
        rv_grid_movies.setHasFixedSize(true);
        rv_grid_movies.setAdapter(moviesAdapter);




        pb.setVisibility(View.VISIBLE);
        dataMovies();

        return rootView;
    }

    private void dataMovies() {
        moviesRepo.getMovies(new OnGetMoviesCallback() {
            @Override
            public void onSuccess(List<MoviesItems> moviesItems) {
                moviesAdapter = new MoviesAdapter(moviesItems);
                rv_grid_movies.setAdapter(moviesAdapter);
                pb.setVisibility(View.GONE);
                moviesAdapter.setClickListener(MoviesFragment.this::itemClicked);
            }

            @Override
            public void onError() {
                Toast.makeText(getActivity(), "check internet", Toast.LENGTH_SHORT).show();
                pb.setVisibility(View.GONE);
            }
        });

    }


    private int dpToPx(int dp) {
        Resources resources = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics()));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void itemClicked(MoviesItems setGet, int position, View v) {
        MoviesItems items = new MoviesItems();
        items.setTitle(setGet.getTitle());
        items.setRelease_date(setGet.getRelease_date());
        items.setOverview(setGet.getOverview());
        items.setPoster_path(setGet.getPoster_path());

        Intent i = new Intent(getActivity(), DetailMoviesActivity.class);
        i.putExtra(DetailMoviesActivity.EXTRA_MOVIES, items);
        startActivity(i);
    }
}
