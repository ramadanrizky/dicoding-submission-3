package com.anggorotri.moviesandtvwapi.repo;

import com.anggorotri.moviesandtvwapi.items_response.MoviesResponse;
import com.anggorotri.moviesandtvwapi.items_response.TVShowResponse;
import com.anggorotri.moviesandtvwapi.retrofit.Api;
import com.anggorotri.moviesandtvwapi.retrofit.OnGetTvShowCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TVShowRepo {

    private static final String LANGUAGE = "en-US";

    private static final  String BASE_URL = "https://api.themoviedb.org/3/discover/";

    private static TVShowRepo tvRepo;

    private Api api;

    private TVShowRepo(Api api){
        this.api = api;
    }

    public static TVShowRepo getInstance(){
        if (tvRepo == null){
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            tvRepo = new TVShowRepo(retrofit.create(Api.class));
        }

        return tvRepo;

    }

    public void getTvShow(final OnGetTvShowCallback tvShowCallback){
        api.getTvShow("c831506e168639cf3884d77eb4de6adc", LANGUAGE, 1)
                .enqueue(new Callback<TVShowResponse>() {
                    @Override
                    public void onResponse(Call<TVShowResponse> call, Response<TVShowResponse> response) {
                        if (response.isSuccessful()){
                            TVShowResponse tvShowResponse = response.body();
                            if (tvShowResponse != null && tvShowResponse.getTvShowItems() != null){
                                tvShowCallback.onSuccess(tvShowResponse.getTvShowItems());
                            }else {
                                tvShowCallback.onError();
                            }
                        }else {
                            tvShowCallback.onError();
                        }
                    }

                    @Override
                    public void onFailure(Call<TVShowResponse> call, Throwable t) {
                        tvShowCallback.onError();
                    }
                });
    }

}
