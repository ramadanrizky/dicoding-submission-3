package com.anggorotri.moviesandtvwapi.repo;

import android.arch.lifecycle.MutableLiveData;

import com.anggorotri.moviesandtvwapi.items_response.MoviesResponse;
import com.anggorotri.moviesandtvwapi.retrofit.Api;
import com.anggorotri.moviesandtvwapi.retrofit.OnGetMoviesCallback;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesRepo {

    private static final String LANGUAGE = "en-US";

    private static final  String BASE_URL = "https://api.themoviedb.org/3/discover/";

    private static MoviesRepo repo;

    private Api api;

    private OnGetMoviesCallback callback;

    private MoviesRepo(Api api) {
        this.api = api;
    }

    public static MoviesRepo getInstance() {
        if (repo == null) {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            repo = new MoviesRepo(retrofit.create(Api.class));
        }

        return repo;
    }

    public void getMovies(final OnGetMoviesCallback callback) {
        api.getMovies("c831506e168639cf3884d77eb4de6adc", LANGUAGE, 1)
                .enqueue(new Callback<MoviesResponse>() {
                    @Override
                    public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
                        if (response.isSuccessful()){
                            MoviesResponse moviesResponse = response.body();
                            if (moviesResponse != null && moviesResponse.getMoviesItems() != null) {
                                callback.onSuccess(moviesResponse.getMoviesItems());
                            }else {
                                callback.onError();
                            }
                        }else {
                            callback.onError();
                        }
                    }

                    @Override
                    public void onFailure(Call<MoviesResponse> call, Throwable t) {
                        callback.onError();
                    }
                });
    }

//    public MutableLiveData<MoviesResponse> getMovies(String key, String language, int page) {
//        MutableLiveData<MoviesResponse> moviesData = new MutableLiveData<>();
//        api.getMovies(key, language, page)
//                .enqueue(new Callback<MoviesResponse>() {
//                    @Override
//                    public void onResponse(Call<MoviesResponse> call, Response<MoviesResponse> response) {
//                        if (response.isSuccessful()) {
//                            MoviesResponse moviesResponse = new MoviesResponse();
//                            if (moviesResponse != null && moviesResponse.getMoviesItems() != null) {
//                                callback.onSuccess(moviesResponse.getMoviesItems());
//                                moviesData.setValue(response.body());
//                            } else {
//                                callback.onError();
//                                moviesData.setValue(null);
//                            }
//                        } else {
//                            callback.onError();
//                            moviesData.setValue(null);
//                        }
//                    }
//
//                    @Override
//                    public void onFailure(Call<MoviesResponse> call, Throwable t) {
//                        callback.onError();
//                        moviesData.setValue(null);
//                    }
//                });
//        return moviesData;
//    }



}



