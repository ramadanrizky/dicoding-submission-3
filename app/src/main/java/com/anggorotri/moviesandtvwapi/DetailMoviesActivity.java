package com.anggorotri.moviesandtvwapi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.anggorotri.moviesandtvwapi.items_response.MoviesItems;
import com.bumptech.glide.Glide;

public class DetailMoviesActivity extends AppCompatActivity {

    public static final String EXTRA_MOVIES = "extra_movies";
    private String url = "https://image.tmdb.org/t/p/original";
    TextView txt_det_title, txt_det_date, txt_det_desc, txt_toolbar;
    ImageView img_detail_movies;
    ProgressBar pb_detail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movies);

        img_detail_movies = findViewById(R.id.img_detail_movie);
        txt_det_title = findViewById(R.id.txt_detail_title);
        txt_det_date = findViewById(R.id.txt_detail_date);
        txt_det_desc = findViewById(R.id.txt_detail_description);
        txt_toolbar = findViewById(R.id.txt_toolbar);
        pb_detail = findViewById(R.id.pb_detail);
        pb_detail.setVisibility(View.VISIBLE);

        getDetailMovie();

    }

    private void getDetailMovie() {
        MoviesItems items = getIntent().getParcelableExtra(EXTRA_MOVIES);
        txt_det_title.setText(items.getTitle());
        txt_det_date.setText(items.getRelease_date());
        txt_det_desc.setText(items.getOverview());
        txt_toolbar.setText(items.getTitle());
        Glide.with(DetailMoviesActivity.this)
                .load(url + items.getPoster_path())
                .into(img_detail_movies);

        pb_detail.setVisibility(View.GONE);
    }
}
