package com.anggorotri.moviesandtvwapi.items_response;

import android.os.Parcel;
import android.os.Parcelable;

public class TVShowItems implements Parcelable {
    private int id;
    private String name;
    private String first_air_date;
    private String overview;
    private String poster_path;

    public TVShowItems(int id, String name, String first_air_date, String overview, String poster_path){
        this.id = id;
        this.name = name;
        this.first_air_date = first_air_date;
        this.overview = overview;
        this.poster_path = poster_path;
    }

    public TVShowItems() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_air_date() {
        return first_air_date;
    }

    public void setFirst_air_date(String first_air_date) {
        this.first_air_date = first_air_date;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.first_air_date);
        dest.writeString(this.overview);
        dest.writeString(this.poster_path);
    }

    protected TVShowItems(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.first_air_date = in.readString();
        this.overview = in.readString();
        this.poster_path = in.readString();
    }

    public static final Parcelable.Creator<TVShowItems> CREATOR = new Parcelable.Creator<TVShowItems>() {
        @Override
        public TVShowItems createFromParcel(Parcel source) {
            return new TVShowItems(source);
        }

        @Override
        public TVShowItems[] newArray(int size) {
            return new TVShowItems[size];
        }
    };
}
