package com.anggorotri.moviesandtvwapi.items_response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MoviesResponse {

    @SerializedName("page")
    @Expose
    private int page;

    @SerializedName("total_results")
    @Expose
    private int  total_results;

    @SerializedName("results")
    @Expose
    private List<MoviesItems> moviesItems;

    @SerializedName("total_pages")
    @Expose
    private int total_pages;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public List<MoviesItems> getMoviesItems() {
        return moviesItems;
    }

    public void setMoviesItems(List<MoviesItems> moviesItems) {
        this.moviesItems = moviesItems;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }
}
