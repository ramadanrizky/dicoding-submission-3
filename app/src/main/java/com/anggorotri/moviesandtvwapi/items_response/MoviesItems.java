package com.anggorotri.moviesandtvwapi.items_response;

import android.os.Parcel;
import android.os.Parcelable;

public class MoviesItems implements Parcelable {

    private int id;
    private String title;
    private String overview;
    private String poster_path;
    private String release_date;

    public MoviesItems(int id, String title, String overview, String poster_path, String release_date ){
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.poster_path = poster_path;
        this.release_date = release_date;
    }

    public MoviesItems() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.overview);
        dest.writeString(this.poster_path);
        dest.writeString(this.release_date);
    }

    protected MoviesItems(Parcel in) {
        this.id = in.readInt();
        this.title = in.readString();
        this.overview = in.readString();
        this.poster_path = in.readString();
        this.release_date = in.readString();
    }

    public static final Creator<MoviesItems> CREATOR = new Creator<MoviesItems>() {
        @Override
        public MoviesItems createFromParcel(Parcel source) {
            return new MoviesItems(source);
        }

        @Override
        public MoviesItems[] newArray(int size) {
            return new MoviesItems[size];
        }
    };
}
